function createInputField(type = "text", value = ""){
	obj = {};
	
	obj.val = value;
	obj.inp = document.createElement("input");
	obj.inp.classList = ["inactive"];
	obj.el = document.createElement("div");
	
	if(type == "root"){
		obj.pre = "√";
	}
	
	obj.el.classList.add(type);
	obj.el.classList.add("inputField");
	
	obj.el.appendChild(obj.inp);
	
	return obj;
}