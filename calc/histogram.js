function setUpHistogram(canvas, context, xMin, xMax, yMin, yMax, width, height, inlcudeLeftEnd = true,){
	let obj = {};

	obj.graph = setUpGraph(canvas, context, xMin, xMax, yMin, yMax, width, height);
	
	obj.graph.context.font = "30px Arial";
	
	obj.clearGraph = function(){
			this.graph.context.clearRect(0, 0, this.graph.width, this.graph.height);
			this.graph.context.font = "30px Arial";
		};
	
	obj.clearGraph();
	
	obj.inlcudeLeftEnd = inlcudeLeftEnd;
	
	obj.makeBottomLabels = function(min, max, numberOfLabels, xaxis){
		
		let spacing = (max - min) / (numberOfLabels - 1);
		
		for( let i = 0; i < numberOfLabels; i++ ){
			let x = i * spacing + min;
			
			let point = this.graph.getPoint(x, 0);
			
			let barWidth = 5 / this.graph.xScale;
			
			this.graph.context.fillRect(point.X - 3, point.Y - 10, 6, 10);
			
			let label = x;
			
			let magnitude = Math.floor(x).toFixed(0).length;
			
			let rounder = 10 ** (5 - magnitude);
			
			label = ( Math.round( x * rounder ) / rounder ) + "";
			
			this.graph.context.fillText(label, point.X - 7, point.Y + 30);
		}
		
		context.save();
		context.textAlign = "center";
		this.graph.context.fillText( xaxis, this.graph.width / 2, this.graph.height - 15);
		
		context.restore();
		
	}
	
	obj.makeLeftLabels = function(min, max, xmin, leftEdge, numberOfLabels, yaxis){
		
		let spacing = (max - min) / (numberOfLabels - 1);
		
		for( let i = 0; i < numberOfLabels; i++ ){
			
			let point = this.graph.getPoint(xmin, i * spacing);
			
			let y = point.Y;
			
			let textx = point.X + 40;
			
			let label = "";
			
			if( max > 9 ){
				label = (i * spacing).toFixed(2) + "%";
				
				if(label.length < 6){
					label = "  " + label;
				}
			} else {
				
				label = (i * spacing).toFixed(3) + "%";
				
			}
			
			let context = this.graph.context;
			
			
			this.graph.context.fillText(label, textx, y + 10);
		
			this.graph.drawXGraph((i * spacing) + "", leftEdge, leftEdge + ( 10 / this.graph.xScale ) );
			
		}
		
		context.save();
		context.translate(0, this.graph.height);
		context.rotate(-Math.PI/2);
		context.textAlign = "center";
		context.fillText(yaxis, 250, 30);
		context.restore();
		
		
	}
	
	obj.drawBar = function(leftEnd, rightEnd, height, filled = false, color="black"){
		this.graph.drawYGraph("" + leftEnd, 0, height, color);
		this.graph.drawXGraph("" + height, leftEnd, rightEnd, filled, color);
		this.graph.drawYGraph("" + rightEnd, 0, height, color);		
	};
	
	obj.drawHistogram = function(data, boundaries, colors = [], xaxis, yaxis){
		
		// If I don't provide a sort function, it sorts the numbers "alphabetically"
		// e.g. 126 comes before 2 because 1 comes before 2 in the "alphabet"
		boundaries.sort(function(x, y){return x - y;});
		data.sort(function(x, y){return x - y;});
		
		let allDataIncluded = false;
		
		// is the smallest data greater than the smallest bound, and the greatest data point less than the greatest bound?
		if( 
			(data[0] > boundaries[0]
			|| ( this.inlcudeLeftEnd && data[0] >= boundaries[0] )
			)
			
			&& (data[data.length - 1] < boundaries[boundaries.length - 1]
			|| ( !this.inlcudeLeftEnd /*include right end*/ && data[data.length - 1] <= boundaries[boundaries.length - 1] )
			)
			
			){
			allDataIncluded = true;
		} /*else {
			false, by default;
		}*/
		
		let bars = [];
		
		for(let i = 0; i < boundaries.length - 1; i++){
			bars[i] = 0;
		}
		
		let lowestBound = 0;
		
		for(let d of data){
			
			if(boundaries.includes(d)){
				
				if(this.inlcudeLeftEnd){
					let boundaryIndex = boundaries.indexOf(d);
					
					if(boundaryIndex == bars.length){
						continue;
					}
					
					d = (boundaries[boundaryIndex] + boundaries[boundaryIndex + 1])/2;
					
				} else {
					let boundaryIndex = boundaries.indexOf(d);
					
					if(boundaryIndex < 0){
						continue;
					}
					
					d = (boundaries[boundaryIndex - 1] + boundaries[boundaryIndex])/2;
				}
				
			}
			
			for(let i = lowestBound; i < bars.length; i++){
				
				if(d > boundaries[i] && d < boundaries[i+1]) {
					bars[i]++;
					lowestBound = i;
					
					break;
				}
				
			}			
			
		}
		
		let tallestBar = 0;
		let preppedBars = [];
		
		for(let i in bars){
			// i here is not numeric. Nor is it a string. I'm not sure what it is, but 1 + 1 = 11 (but not the string.)
			
			let index = i * 1;
			
			let leftEnd = boundaries[index];
			let rightEnd = boundaries[index + 1];
			let height = ( (bars[index] * 100) / data.length ) / (rightEnd - leftEnd);
			
			if(tallestBar < height){
				tallestBar = height;
			}
			
			preppedBars.push({
				left: leftEnd,
				right: rightEnd,
				height: height
			});
			
		}
		
		console.log(preppedBars);
		
		return this.formHistoFromBars(preppedBars, boundaries, colors, xaxis, yaxis, tallestBar, allDataIncluded);
		
	}
	
	obj.formHistoFromBars = function(preppedBars, boundaries, colors, xaxis, yaxis, tallestBar, allDataIncluded = true){
		
		let histoYMax = tallestBar * 1.1;
		let histoYMin = tallestBar * - 0.2;
		
		let histXRange = boundaries[boundaries.length - 1] - boundaries[0];
		
		let histoXMax = boundaries[boundaries.length - 1] + histXRange * .2;
		let histoXMin = boundaries[0] - histXRange * .35;
		
		this.graph = reboundGraph(this.graph, histoXMin, histoXMax, histoYMin, histoYMax);
		this.clearGraph();
		
		let leftEdge = boundaries[0] - histXRange * .1; 
		
		this.graph.drawXGraph("0", leftEdge, histoXMax);
		
		this.makeBottomLabels(boundaries[0], boundaries[boundaries.length - 1], 6, xaxis);
		
		for(let i = 0; i < preppedBars.length; i++){
			
			let b = preppedBars[i];
			
			if(i < colors.length && colors[i] !== "#ffffff"){
				this.drawBar(b.left, b.right, b.height, true, colors[i]);
			} else {
				this.drawBar(b.left, b.right, b.height);
			}
		}
		
		this.graph.drawYGraph("" + leftEdge, 0, histoYMax);
		
		this.makeLeftLabels(0, tallestBar, histoXMin, leftEdge, 5, yaxis);
		
		return allDataIncluded;
		
	};
	
	return obj;
}