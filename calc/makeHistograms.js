const datapage = document.getElementById("datapage");
const dataEl = document.getElementById("data");
const freDataContainer = document.getElementById("fre");
const FreHead = document.getElementById("FreHead");
const genOptions = document.getElementById("genOptions");
const resultsEl = document.getElementById("results");
const averageEl = document.getElementById("average");
const numBarsInp = document.getElementById("numberOfBars");
const numBarsErr = document.getElementById("numBarsError");
const sdEl = document.getElementById("sd");
const sdPlusEl = document.getElementById("sdplus");
const staticData = document.getElementById("staticData");
const tbody = document.getElementById("histoData").children[0];
const fretbody = document.getElementById("histoDataFre").children[0];
const staticTbody = document.getElementById("staticData");
const endpointEl = document.getElementById("endPointConvention");
const boundariesDiv = document.getElementById("boundariesDiv");
const boundariesInnerDiv = document.getElementById("boundariesInnerDiv");
const boundErrDiv = document.getElementById("boundErrorDiv");
const colorsDiv = document.getElementById("colorsDiv");
const colorsInnerDiv = document.getElementById("colorsInnerDiv");
const missingDataError = document.getElementById("missingDataError");
const missingFreError = document.getElementById("missingFreError");
const boundsWarning = document.getElementById("boundsWarning");
const histogram = document.getElementById("histogram");
const xaxisinput = document.getElementById("xaxis");
const yaxisinput = document.getElementById("yaxis");
const inputOptions = document.getElementById("inputOptions");
const rawDataResults = document.getElementById("rawDataResults");

function showinputs(){
	let type = inputOptions.value;
	
	boundariesInnerDiv.innerHTML = "";
	colorsInnerDiv.innerHTML = "";
	
	dataEl.classList.add("hide");
	freDataContainer.classList.add("hide");
	
	genOptions.classList.remove("hide");
	
	if(type == 'raw'){
		dataEl.classList.remove("hide");
	} else if(type == 'fre'){
		FreHead.textContent = "Frequency";
		freDataContainer.classList.remove("hide");
	} else {
		FreHead.textContent = "Percent";
		freDataContainer.classList.remove("hide");
	}
	
}

var context = histogram.getContext("2d");

var myHisto = setUpHistogram(histogram, context, 0, 3, 0, 5, 900, 500);

function editData(){
	resultsEl.classList.add("hide");
	datapage.classList.remove("hide");
}

function autoFit(){
	boundariesInnerDiv.innerHTML = "";
	boundErrDiv.innerText = "";
	boundariesDiv.classList.add("hide");
	
	analyzeData();
}

function freqToHisto(){
	
	staticTbody.innerHTML = "";
	
	let tallestBar = 0;
	let preppedBars = [];
	let totalFreq = 0;
	let boundaries = [];
	
	missingFreError.textContent = "";
	
	let rows = 0;
	
	for( let tr of fretbody.children ){
		
		if(tr.children[0].children.length < 1){
			// header row
			
			let newRow = tr.cloneNode(true);
			
			staticTbody.appendChild(newRow);
			
			continue;
		}
		
		rows++;
		
		for( let td of tr.children){
			if(td.children[0].value.length < 1){
				missingFreError.textContent = "All inputs must have data before you continue.";
				return;
			}
		}
		
		let newRow = tr.cloneNode(true);
		
		let leftBound = tr.children[0].children[0].value * 1;
		let rightBound = tr.children[1].children[0].value * 1;
		let barFreq = tr.children[2].children[0].value * 1;
		
		if( inputOptions.value == "per" ){
			newRow.children[0].innerHTML = tr.children[0].children[0].value;
			newRow.children[1].innerHTML = tr.children[1].children[0].value;
			newRow.children[2].innerHTML = tr.children[2].children[0].value + "%";
			
		} else {
			newRow.children[0].innerHTML = tr.children[0].children[0].value * 1;
			newRow.children[1].innerHTML = tr.children[1].children[0].value * 1;
			newRow.children[2].innerHTML = tr.children[2].children[0].value * 1;
		
		}
		
		if(leftBound >= rightBound){
			missingFreError.textContent = `The left bound in row ${rows} is not less than the right bound.`;
			return;
		}
		
		staticTbody.appendChild(newRow);
		
		if(boundaries.length == 0){
			boundaries.push(leftBound);
		}
		
		boundaries.push(rightBound);
		
		totalFreq += barFreq;
		
	}
	
	while(colorsInnerDiv.children.length < rows){
		let newInp = document.createElement("input");
		newInp.type = "color";
		newInp.value = "#ffffff";
		
		let newDiv = document.createElement("div");
		
		newDiv.appendChild(newInp);
		
		colorsInnerDiv.appendChild(newDiv);
	}
	
	while(colorsInnerDiv.children.length > rows){
		colorsInnerDiv.children[colorsInnerDiv.children.length - 1].remove();
	}
	
	if(Math.abs(100 - totalFreq) > 0.1 && inputOptions.value == "per"){
		missingFreError.textContent = "The \"Percent\" column must sum to exactly 100%.";
		return;
	}
	
	for( let tr of fretbody.children ){
		
		if(tr.children[0].children.length < 1){
			// header row
			continue;
		}
		
		let leftBound = tr.children[0].children[0].value * 1;
		let rightBound = tr.children[1].children[0].value * 1;
		let barFreq = tr.children[2].children[0].value * 1;
		
		let barHeight = ( ( barFreq / totalFreq ) * 100 ) / ( rightBound - leftBound );
		
		if( barHeight > tallestBar ){
			tallestBar = barHeight;
		}
		
		preppedBars.push({
			left: leftBound,
			right: rightBound,
			height: barHeight
		});
		
	}
	
	myHisto.formHistoFromBars(preppedBars, boundaries, getColors(), xaxisinput.value, yaxisinput.value, tallestBar, true);
	
	datapage.classList.add("hide");
	resultsEl.classList.remove("hide");
	window.scrollTo(0,0);
	
}

function analyzeData(){
	
	myHisto.inlcudeLeftEnd = endpointEl.value == "left";
	
	staticTbody.innerHTML = "";
	
	let data = [];
	let max = null;
	let min = null;
	dataSum = {};
	
	if(inputOptions.value !== "raw"){
		rawDataResults.classList.add("hide");
		freqToHisto();
		return;
	}
	
	rawDataResults.classList.remove("hide");
	
	for( let tr of tbody.children ){
		
		for(let td of tr.children){
			if(td.children.length > 0){
				// the td has an input. Get its value and make it static.
				
				if(td.children[0].value.length < 1){
					missingDataError.innerHTML = "All inputs must have data before you continue.";
					return;
				}
				
				let val = td.children[0].value * 1;
				data.push(val * 1);
				
				if(dataSum[val] == undefined) {
					dataSum[val] = 1;
				} else {
					dataSum[val]++;
				}
				
				if(max == null || val > max){
					max = val;
				}
				
				if(min == null || val < min){
					min = val;
				}
				
			}
		}
		
	}
	
	let tr = document.createElement("tr");
	let dataHeader = document.createElement("td");
	let frequencyHeader = document.createElement("td");
	
	dataHeader.textContent = "Data";
	frequencyHeader.textContent = "Frequency";
	
	tr.appendChild(dataHeader);
	tr.appendChild(frequencyHeader);
	
	staticTbody.appendChild(tr);
	
	for( let d of Object.keys(dataSum).sort(function(x, y){return x - y;}) ){
		
		let tr = document.createElement("tr");
		let dataTD = document.createElement("td");
		let frequencyTD = document.createElement("td");
		
		dataTD.textContent = d;
		frequencyTD.textContent = dataSum[d];
		
		tr.appendChild(dataTD);
		tr.appendChild(frequencyTD);
		
		staticTbody.appendChild(tr);
		
		
	}	
	
	missingDataError.innerHTML = "";
	
	let dataAverage = average(data);
	let dataSD = sd(data);
	let dataSDPlus = sdPlus(data);
	
	averageEl.textContent = dataAverage.toFixed(4);
	sdEl.textContent = dataSD.toFixed(4);
	sdPlusEl.textContent = dataSDPlus.toFixed(4);
	
	let numBars = numBarsInp.value * 1;
	
	let boundaries = [];
	
	if(boundariesInnerDiv.children.length < 1){
		
		let innerHTML = "<div>";
		
		let width = (max * 1 - min * 1)/(numBars - 1);
		//console.log(`max: ${max} min: ${min} width: ${width}`);
		
		let lastBound = min * 1;
		
		if( !myHisto.inlcudeLeftEnd ){
			lastBound -= width;
		}
		
		boundaries.push(lastBound);
		
		innerHTML += `Bar 1: <input type='number' value='${lastBound.toFixed(2)}' />`;
		
		let newColorInps = "";
		
		for(let i = 0; i < numBars; i++){
			lastBound += width;
			
			newColorInps += "<div><input type='color' value='#ffffff' /></div>"
			
			if( i == numBars - 1){
				innerHTML += `~<input value='${lastBound.toFixed(2)}' /></div>`;
			} else {
				innerHTML += `~<input onkeyup='changeNextDiv(this);' value='${lastBound.toFixed(2)}' /></div><div>Bar ${i + 2}: <input type='number' onkeyup='changePreviousDiv(this);' value='${lastBound.toFixed(2)}' />`;
			}
			
			boundaries.push(lastBound.toFixed(2) * 1);
		}
		
		if(colorsInnerDiv.children.length < 1){
			colorsInnerDiv.innerHTML = newColorInps;
		}
		
		boundariesInnerDiv.innerHTML = innerHTML;
		
	} else {
		
		for(let div of boundariesInnerDiv.children){
			
			if( boundaries.length < 1 ){
				boundaries.push(div.children[0].value * 1);
			}
			
			if(div.getElementsByTagName("input").length > 1){
				boundaries.push(div.children[1].value * 1);
			}
			
		}
		
	}
	
	getColors();
	
	let hasAllData = myHisto.drawHistogram(data, boundaries, colors, xaxisinput.value, yaxisinput.value);
	
	if(hasAllData){
		boundsWarning.innerText = "";
	} else {
		boundsWarning.innerText = "Not all data are included in the boundaries. Adjust the bounds or press \"Autosize graph\".";
	}
	
	datapage.classList.add("hide");
	resultsEl.classList.remove("hide");
	window.scrollTo(0,0);
	
}

function addRow(elId){
	let table = document.getElementById(elId).children[0];
	let rows = table.getElementsByTagName("tr");
	let lastRow = rows[rows.length - 1];
	
	let newRow = lastRow.cloneNode(true);
	
	let values = ['','','','','','','','','','','','',''];
	
	let inputs = newRow.getElementsByTagName("input");
	
	if(inputs.length > 2){
		values[0] = inputs[1].value;
		values[1] = inputs[1].value * 1 + 1;
	}
	
	for(let inpIndex = 0; inpIndex < inputs.length; inpIndex++){
		inputs[inpIndex].value = values[inpIndex];
	}
	
	table.appendChild(newRow);
}

function removeRow(elId){
	let table = document.getElementById(elId);
	
	let rows = table.getElementsByTagName("tr");
	
	let lastRow = rows[rows.length - 1];
	
	if(rows.length < 2){
		alert("You cannot remove the only row of data.");
		return;
	} else if(
			lastRow.getElementsByTagName("input")[0].value.length < 1
			|| confirm("Would you like to remove the last row of data?")
		)
			{
		
		lastRow.remove();
	}
	
}

function editNumBars(){

	
	if(numBarsInp.value < 1){
		numBarsErr.innerText = "You must have at least one bar.";
		return;
	}
	
	while(boundariesInnerDiv.children.length > numBarsInp.value){
		
		let lastDiv = boundariesInnerDiv.children[boundariesInnerDiv.children.length - 1];
		let lastDivLastInp = lastDiv.children[1];
		
		let bound = lastDivLastInp.value;
		
		boundariesInnerDiv.children[boundariesInnerDiv.children.length - 1].remove();
		
		let newLastDiv = boundariesInnerDiv.children[boundariesInnerDiv.children.length - 1];
		
		newLastDiv.children[1].onkeyup = function(){};
		
		colorsInnerDiv.children[colorsInnerDiv.children.length - 1].remove();
		
	}
	
	while(boundariesInnerDiv.children.length < numBarsInp.value){
		
		let lastDiv = boundariesInnerDiv.children[boundariesInnerDiv.children.length - 1];
		let lastDivLastInp = lastDiv.children[1];
		
		let bound = lastDivLastInp.value;
		
		if(typeof bound == "undefined"){
			bound = 0;
		}
		
		lastDivLastInp.onkeyup = changeNextDiv;
		
		let div = document.createElement("div");
		
		div.innerHTML = `Bar ${boundariesInnerDiv.children.length + 1}: <input type='number' onkeyup='changePreviousDiv(this)' value='${bound}' />~<input value='' />`;
		
		boundariesInnerDiv.appendChild(div);
		
		let newColorInp = document.createElement("input");
		
		newColorInp.type = "color";
		newColorInp.value = "#ffffff";
		
		let newColorDiv = document.createElement("div");
		
		newColorDiv.appendChild(newColorInp);
		
		colorsInnerDiv.appendChild(newColorDiv);
		
	}
	
	numBarsErr.innerText = "";
	
}

function showBounds(){
	boundariesDiv.classList.remove("hide");
	resultsEl.classList.add("hide");
}

function showColors(){
	colorsDiv.classList.remove("hide");
	resultsEl.classList.add("hide");
}

function colorHistogram(){
	colorsDiv.classList.add("hide");
	resultsEl.classList.remove("hide");
	analyzeData();
}

function showHistogram(){
	
	let val1 = boundariesInnerDiv.children[0].children[0].value;
	let val2 = false;
	
	//if(boundariesInnerDiv.children.length > 1){
		val2 = boundariesInnerDiv.children[0].children[1].value;
	//} else {
		// only one row of boundaries
		//val2 = boundariesInnerDiv.children[0].children[1].value;
	//}
	
	let barCounter = 1;
	
	for(let div of boundariesInnerDiv.children){
		let val1 = div.children[0].value;
		let val2 = div.children[1].innerText;
		
		if(div.getElementsByTagName("input").length > 1){
			val2 = div.children[1].value;
		}
		
		if(val2.length < 1) {
			boundErrDiv.innerText = `The left bound of bar ${barCounter + 1} is missing.`;
			return;
		}
		
		if(parseFloat(val1) >= parseFloat(val2)){
			boundErrDiv.innerText = `The left bound of bar ${barCounter} needs to less than the right bound.`;
			return;
		}
		
		barCounter++;
		
	}
	
	boundErrDiv.innerText = "";
	
	boundariesDiv.classList.add("hide");
	resultsEl.classList.remove("hide");
	analyzeData();
}

function changeNextRow(input, event){
	let nextRow = input.parentElement.parentElement.nextElementSibling;
	
	if(!nextRow){
		return;
	}
	
	let bound = input.value * 1;
	
	nextRow.children[0].children[0].value = bound;
}

function changePreviousRow(input, event){
	let prevRow = input.parentElement.parentElement.previousElementSibling;
	
	if(prevRow.children[1].children.length < 1){
		return;
	}
	
	let bound = input.value * 1;
	
	prevRow.children[1].children[0].value = bound;
}

function changePreviousDiv(input){
	
	if(!input.parentElement){
		input = this;
	}
	
	let prevDiv = input.parentElement.previousElementSibling;
	
	let bound = input.value * 1;
	
	prevDiv.children[1].value = bound;
	
}

function changeNextDiv(input){
	
	if(!input.parentElement){
		input = this;
	}
	
	let nextDiv = input.parentElement.nextElementSibling;
	
	let bound = input.value * 1;
	
	nextDiv.children[0].value = bound;
	
}

function getColors(){
	colors = [];
	
	for( let div of colorsInnerDiv.children ){
		colors.push(div.children[0].value);
	}
	
	return colors;
}

